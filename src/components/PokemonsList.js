import React, { useContext, useEffect } from "react";

import { PokemonContext } from "./PokemonProvider";

const url = "https://pokeapi.co/api/v2/pokemon?limit=151";

const PokemonsList = () => {
  const { pokemons, capture, addPokemons } = useContext(PokemonContext);

  useEffect(() => {
    const fetchPokemons = async () => {
      const response = await fetch(url);
      const data = await response.json();
      addPokemons(data.results);
    };

    fetchPokemons();
  }, [addPokemons]);

  return (
    <div className="pokemons-list">
      <h2>Wild Pokemons</h2>

      {pokemons.map((pokemon) => (
        <div key={`${pokemon.id}-${pokemon.name}`}>
          <div>
            <span>{pokemon.name}</span>
            <button onClick={capture(pokemon)}>+</button>
          </div>
        </div>
      ))}
    </div>
  );
};

export default PokemonsList;
