import React from "react";
import "./App.css";

import PokemonsList from "./components/PokemonsList";
import CapturedPokemons from "./components/CapturedPokemons";
// import PokemonForm from "./components/PokemonForm";
import { PokemonProvider } from "./components/PokemonProvider";

const App = () => (
  <PokemonProvider>
    <div className="App">
      <PokemonsList />
      <CapturedPokemons />
    </div>
    {/* <PokemonForm /> */}
  </PokemonProvider>
);

export default App;
