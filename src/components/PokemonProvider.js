import React, { createContext, useReducer } from "react";
import {
  pokemonReducer,
  CAPTURE,
  RELEASE,
  ADD_POKEMON,
  ADD_POKEMONS,
} from "./PokemonReducer";

export const PokemonContext = createContext();

export const PokemonProvider = (props) => {
  const defaultState = {
    pokemons: [
      { id: 1, name: "Bulbasaur" },
      { id: 2, name: "Charmander" },
      { id: 3, name: "Squirtle" },
    ],
    capturedPokemons: [],
  };

  const [state, dispatch] = useReducer(pokemonReducer, defaultState);

  const capture = (pokemon) => () => {
    dispatch({ type: CAPTURE, pokemon });
  };

  const release = (pokemon) => () => {
    dispatch({ type: RELEASE, pokemon });
  };

  const addPokemon = (pokemon) => {
    dispatch({ type: ADD_POKEMON, pokemon });
  };

  const addPokemons = (pokemons) => {
    dispatch({ type: ADD_POKEMONS, pokemons });
  };

  const { pokemons, capturedPokemons } = state;

  const providerValue = {
    pokemons,
    capturedPokemons,
    release,
    capture,
    addPokemon,
    addPokemons,
  };

  return (
    <PokemonContext.Provider value={providerValue}>
      {props.children}
    </PokemonContext.Provider>
  );
};
